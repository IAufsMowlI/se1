package labyrinth;

/**
 * @author Florian Erwerth
 */
public class Cell {

    private int row;
    private int column;
    private boolean wallDown = true;
    private boolean wallRight = true;
    private boolean visited = false;

    /**
     * @param row    is the number of the row the cell is in
     * @param column is the number of the column the cell is in
     */
    public Cell(int row, int column) {

        this.row = row;
        this.column = column;


    }

    /**
     * gets the row of the cell.
     *
     * @return the row.
     */

    public int getRow() {
        return row;
    }

    /**
     * sets the row of the cell.
     *
     * @param row is the row the cell is now in
     */
    public void setRow(int row) {
        this.row = row;
    }

    /**
     * gets the column of the cell.
     *
     * @return the column.
     */

    public int getColumn() {
        return column;
    }

    /**
     * sets the column the cell is in.
     *
     * @param column of the cell.
     */

    public void setColumn(int column) {
        this.column = column;
    }

    /**
     * Checks if the wall on the bottom of the cell is there (true) or not (false).
     *
     * @return true if the wall is there and false if not.
     */

    public boolean isWallDown() {
        return this.wallDown;
    }

    /**
     * Sets the wall on the bottom to false if the wall is not there anymore.
     * Sets the wall on the bottom to true if the wall is there.
     *
     * @param wallDown is the boolean for the bottom wall.
     */

    public void setWallDown(boolean wallDown) {
        this.wallDown = wallDown;
    }

    /**
     * Checks if the wall on the right of the cell is there (true) or not (false).
     *
     * @return true if the wall is there and false if not.
     */

    public boolean isWallRight() {
        return wallRight;
    }

    /**
     * Sets the wall on the right hand side to false if the wall is not there anymore.
     * Sets the wall on the right hand side to true if the wall is there.
     *
     * @param wallRight is the boolean for the wall on the right hand side.
     */

    public void setWallRight(boolean wallRight) {
        this.wallRight = wallRight;
    }

    /**
     * Checks whether a cell is visited or not.
     *
     * @return true if the cell has been visited and false if the cell has'nt been visited.
     */

    public boolean isVisited() {
        return visited;

    }

    /**
     * Sets the cell to isVisited = true, if it has been visited.
     *
     * @param visited is true if the cell has been visited.
     */

    public void setVisited(boolean visited) {
        this.visited = visited;

    }

    /**
     *
     */

    public void visit() {

        this.visited = true;

    }

}
