package labyrinth;

import java.util.Random;

/**
 * @author Florian Erwerth
 */
public class Labyrinth {

    private Random random = new Random();
    private int width;
    private int height;
    private Cell[][] cells;

    /**
     * @param height Is the height of the labyrinth.
     * @param width  Is the width of the labyrinth.
     */


    Labyrinth(int height, int width) {

        this.width = width;
        this.height = height;
        this.cells = new Cell[height][width];

        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {

                cells[row][column] = new Cell(row, column);

            }
        }

    }

    /**
     * Height of the labyrinth.
     *
     * @return height of the labyrinth
     */
    public int getHeight() {
        return height;
    }

    /**
     * Gets the width of the labyrinth.
     *
     * @return the width of the labyrinth.
     */

    public int getWidth() {
        return width;
    }

    /**
     * The method gets the position of the cells in the labyrinth.
     *
     * @param height is y position of the cells.
     * @param width  is x position of the cells.
     * @return null
     */

    public Cell getCell(int height, int width) {
        return this.cells[height][width];

    }

    /**
     * The method gets the current neighbors of the cells depending of the row and
     * the column of the current cells.
     * <p>
     * It creates a two dimensional array of neighbors and checks if the cells
     * nearby are not null. If this is the case it adds the cells to the array
     * neighbors.
     * <p>
     * The maximum elements that are not null are 4, because a cells can have max 4
     * neighbors.
     * <p>
     * The method itself prevents the danger of one cells in two indexes of the array
     * neighbors.
     *
     * @param cell of the labyrinth.
     * @return the list of the neighbors of the cells.
     */

    Cell[] getNeighbors(Cell cell) {

        Cell[] possibleNeighbors = new Cell[4];
        int countingOfNeighbors = 0;

        if (cell.getRow() != getHeight() - 1) {
            possibleNeighbors[countingOfNeighbors] = this.getCell(cell.getRow() + 1, cell.getColumn());
            countingOfNeighbors += 1;
        }
        if (cell.getColumn() != 0) {
            possibleNeighbors[countingOfNeighbors] = this.getCell(cell.getRow(), cell.getColumn() - 1);
            countingOfNeighbors += 1;
        }
        if (cell.getColumn() != getWidth() - 1) {
            possibleNeighbors[countingOfNeighbors] = this.getCell(cell.getRow(), cell.getColumn() + 1);
            countingOfNeighbors += 1;
        }
        if (cell.getRow() != 0) {
            possibleNeighbors[countingOfNeighbors] = this.getCell(cell.getRow() - 1, cell.getColumn());
            countingOfNeighbors += 1;
        }

        Cell[] neighbors = new Cell[countingOfNeighbors];

        for (int i = 0; i < countingOfNeighbors; i++) {

            neighbors[i] = possibleNeighbors[i];

        }
        return neighbors;
    }

    /**
     * Gets the neighbor of the current cells and checks, if the cells nearby is
     * visited or not. If its not visited, it adds the cells nearby as neighbor in
     * the array list.
     *
     * @param cell    of the labyrinth.
     * @param visited is the status of the cells.
     * @return the list of the unvisited neighbors, if the length of the array is
     * not longer than the possible entries of it.
     */

    Cell[] getNeighbors(Cell cell, boolean visited) {

        Cell[] possibleNeighbors = new Cell[4];
        int countedCells = 0;

        for (int i = 0; i < getNeighbors(cell).length; i++) {
            if (getNeighbors(cell)[i].isVisited() == visited) {
                possibleNeighbors[countedCells] = getNeighbors(cell)[i];
                countedCells += 1;
            }
        }

        Cell[] neighbors = new Cell[countedCells];

        for (int j = 0; j < countedCells; j++) {
            neighbors[j] = possibleNeighbors[j];

        }
        return neighbors;
    }

    /**
     * It is the method which is the kill part of the algorithm.
     *
     * @param cell of the labyrinth
     */

    void walkRandom(Cell cell) {

        Cell otherCell = randomUnvisitedNeighbor(cell);

        while (otherCell != null) {
            if (otherCell == this.cells[cell.getRow() + 1][cell.getRow()]
                || otherCell == this.cells[cell.getRow()][cell.getRow() - 1]) {
                if (otherCell == this.cells[cell.getRow() + 1][cell.getRow()]) {
                    otherCell.setWallDown(false);
                    otherCell.visit();
                }
                if (otherCell == this.cells[cell.getRow()][cell.getRow() - 1]) {
                    otherCell.setWallRight(false);
                    otherCell.visit();
                }
            }
            if (otherCell == this.cells[cell.getRow() - 1][cell.getColumn()]
                || otherCell == this.cells[cell.getRow()][cell.getColumn() + 1]) {
                if (otherCell == this.cells[cell.getRow() - 1][cell.getColumn()]) {
                    cell.setWallDown(false);
                    otherCell.visit();
                }
                if (otherCell == this.cells[cell.getRow()][cell.getColumn() + 1]) {
                    cell.setWallRight(false);
                    otherCell.visit();
                }
            }
            cell = otherCell;
            cell.visit();
            otherCell = randomUnvisitedNeighbor(cell);

            if (otherCell == null) {

                break;

            }

        }
    }


    Cell hunt() {

        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {

                if (!(this.cells[row][column].isVisited()) && (this.getNeighbors(this.cells[row][column], true)).length > 0) {
                    //length damit geprueft wird ob es eintragungen im getNeighbor array gibt fuer die das gilt
                    Cell visitedNeighbor = this.randomVisitedNeighbor(cells[row][column]);

                    if (this.getCell(row, column) == this.getCell(visitedNeighbor.getRow() + 1, visitedNeighbor.getColumn())) {

                        visitedNeighbor.setWallDown(false);
                        this.cells[row][column].visit();

                    }

                    if (this.getCell(row, column) == this.getCell(visitedNeighbor.getRow(), visitedNeighbor.getColumn() - 1)) {

                        visitedNeighbor.setWallRight(false);
                        this.cells[row][column].visit();
                    }

                    if (this.getCell(row, column) == this.getCell(visitedNeighbor.getRow(), visitedNeighbor.getColumn() + 1)) {

                        this.cells[row][column].setWallRight(false);
                        this.cells[row][column].visit();

                    }

                    if (this.getCell(row, column) == this.getCell(visitedNeighbor.getRow() - 1, visitedNeighbor.getColumn())) {

                        this.cells[row][column].setWallDown(false);
                        this.cells[row][column].visit();

                    }

                }

            }
        }
        return null;
    }


    /**
     * @param cell is the cells array that are neighbors.
     * @return A cells with random location in row and column.
     */

    Cell randomUnvisitedNeighbor(Cell cell) {
        if (this.getNeighbors(cell, false).length != 0) {
            return this.getNeighbors(cell, false)[random.nextInt(this.getNeighbors(cell, false).length)];
        }
        return null;
    }

    /**
     * @param cell
     * @return
     */
    Cell randomVisitedNeighbor(Cell cell) {
        if (this.getNeighbors(cell, true).length != 0) {
            return this.getNeighbors(cell, true)[this.random.nextInt(this.getNeighbors(cell, true).length)];
        }
        return null;
    }


    /**
     *
     */
    public void createLabyrinth() {

        Cell startingCell = this.cells[this.random.nextInt(this.getHeight())][this.random.nextInt(this.getWidth())];
        startingCell.visit();

        while (startingCell != null) {

            this.walkRandom(startingCell);
            startingCell = this.hunt();
        }
    }
}
