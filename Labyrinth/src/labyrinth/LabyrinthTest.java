package labyrinth;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class LabyrinthTest {


    private static final int LAB_HEIGHT = 6;
    private static final int LAB_WIDTH = 7;
    private Labyrinth labyrinth = new Labyrinth(LAB_HEIGHT, LAB_WIDTH);

    @Test
    void testGetWidth() {
        Assertions.assertEquals(7, labyrinth.getWidth());
    }

    @Test
    void testGetHeight() {
        Assertions.assertEquals(6, labyrinth.getHeight());
    }

    @Test
    void testGetCell() {
        Cell testCell = new Cell(3, 4);
        Assertions.assertEquals(testCell.getColumn(), labyrinth.getCell(3, 4).getColumn());
        Assertions.assertEquals(testCell.getRow(), labyrinth.getCell(3, 4).getRow());
        Assertions.assertEquals(testCell.isVisited(), labyrinth.getCell(3, 4).isVisited());
        Assertions.assertEquals(testCell.isWallRight(), labyrinth.getCell(3, 4).isWallRight());
        Assertions.assertEquals(testCell.isWallDown(), labyrinth.getCell(3, 4).isWallDown());
    }

    @Test
    void testGetNeighbors() {
        Cell testCell10 = labyrinth.getCell(1, 1);
        Cell[] testcells = {
            labyrinth.getCell(0, 1),
            labyrinth.getCell(2, 1),
            labyrinth.getCell(1, 0),
            labyrinth.getCell(1, 2),
        };

        Assertions.assertEquals(testcells[0].getRow(), labyrinth.getNeighbors(testCell10)[0].getRow());
        Assertions.assertEquals(testcells[0].getColumn(), labyrinth.getNeighbors(testCell10)[0].getColumn());
        Assertions.assertEquals(testcells[0].isVisited(), labyrinth.getNeighbors(testCell10)[0].isVisited());
        Assertions.assertEquals(testcells[0].isWallRight(), labyrinth.getNeighbors(testCell10)[0].isWallRight());
        Assertions.assertEquals(testcells[0].isWallDown(), labyrinth.getNeighbors(testCell10)[0].isWallDown());
        Assertions.assertEquals(testcells[1].getRow(), labyrinth.getNeighbors(testCell10)[1].getRow());
        Assertions.assertEquals(testcells[1].getColumn(), labyrinth.getNeighbors(testCell10)[1].getColumn());
        Assertions.assertEquals(testcells[1].isVisited(), labyrinth.getNeighbors(testCell10)[1].isVisited());
        Assertions.assertEquals(testcells[1].isWallRight(), labyrinth.getNeighbors(testCell10)[1].isWallRight());
        Assertions.assertEquals(testcells[1].isWallDown(), labyrinth.getNeighbors(testCell10)[1].isWallDown());
        Assertions.assertEquals(testcells[2].getRow(), labyrinth.getNeighbors(testCell10)[2].getRow());
        Assertions.assertEquals(testcells[2].getColumn(), labyrinth.getNeighbors(testCell10)[2].getColumn());
        Assertions.assertEquals(testcells[2].isVisited(), labyrinth.getNeighbors(testCell10)[2].isVisited());
        Assertions.assertEquals(testcells[2].isWallRight(), labyrinth.getNeighbors(testCell10)[2].isWallRight());
        Assertions.assertEquals(testcells[2].isWallDown(), labyrinth.getNeighbors(testCell10)[2].isWallDown());
        Assertions.assertEquals(testcells[3].getRow(), labyrinth.getNeighbors(testCell10)[3].getRow());
        Assertions.assertEquals(testcells[3].getColumn(), labyrinth.getNeighbors(testCell10)[3].getColumn());
        Assertions.assertEquals(testcells[3].isVisited(), labyrinth.getNeighbors(testCell10)[3].isVisited());
        Assertions.assertEquals(testcells[3].isWallRight(), labyrinth.getNeighbors(testCell10)[3].isWallRight());
        Assertions.assertEquals(testcells[3].isWallDown(), labyrinth.getNeighbors(testCell10)[3].isWallDown());
    }

    @Test
    void testGetNeighbors1() {

        Cell testCell11 = labyrinth.getCell(1, 1);
        Cell[] testcells = {
            labyrinth.getCell(0, 1),
            labyrinth.getCell(2, 1),
            labyrinth.getCell(1, 0),
            labyrinth.getCell(1, 2),
        };
        labyrinth.getCell(0, 1).visit();
        labyrinth.getCell(1, 2).visit();

        Assertions.assertEquals(testcells[1].getRow(), labyrinth.getNeighbors(testCell11, false)[0].getRow());
        Assertions.assertEquals(testcells[1].getColumn(), labyrinth.getNeighbors(testCell11, false)[0].getColumn());
        Assertions.assertEquals(testcells[1].isVisited(), labyrinth.getNeighbors(testCell11, false)[0].isVisited());
        Assertions.assertEquals(testcells[1].isWallRight(), labyrinth.getNeighbors(testCell11, false)[0].isWallRight());
        Assertions.assertEquals(testcells[1].isWallDown(), labyrinth.getNeighbors(testCell11, false)[0].isWallDown());
        Assertions.assertEquals(testcells[2].getRow(), labyrinth.getNeighbors(testCell11, false)[1].getRow());
        Assertions.assertEquals(testcells[2].getColumn(), labyrinth.getNeighbors(testCell11, false)[1].getColumn());
        Assertions.assertEquals(testcells[2].isVisited(), labyrinth.getNeighbors(testCell11, false)[1].isVisited());
        Assertions.assertEquals(testcells[2].isWallRight(), labyrinth.getNeighbors(testCell11, false)[1].isWallRight());
        Assertions.assertEquals(testcells[2].isWallDown(), labyrinth.getNeighbors(testCell11, false)[1].isWallDown());
        Assertions.assertEquals(testcells[0].getRow(), labyrinth.getNeighbors(testCell11, true)[0].getRow());
        Assertions.assertEquals(testcells[0].getColumn(), labyrinth.getNeighbors(testCell11, true)[0].getColumn());
        Assertions.assertEquals(testcells[0].isVisited(), labyrinth.getNeighbors(testCell11, true)[0].isVisited());
        Assertions.assertEquals(testcells[0].isWallRight(), labyrinth.getNeighbors(testCell11, true)[0].isWallRight());
        Assertions.assertEquals(testcells[0].isWallDown(), labyrinth.getNeighbors(testCell11, true)[0].isWallDown());
        Assertions.assertEquals(testcells[3].getRow(), labyrinth.getNeighbors(testCell11, true)[1].getRow());
        Assertions.assertEquals(testcells[3].getColumn(), labyrinth.getNeighbors(testCell11, true)[1].getColumn());
        Assertions.assertEquals(testcells[3].isVisited(), labyrinth.getNeighbors(testCell11, true)[1].isVisited());
        Assertions.assertEquals(testcells[3].isWallRight(), labyrinth.getNeighbors(testCell11, true)[1].isWallRight());
        Assertions.assertEquals(testcells[3].isWallDown(), labyrinth.getNeighbors(testCell11, true)[1].isWallDown());
    }

    @Test
    void testHunt() {
        labyrinth.createLabyrinth();
    }

    @Test
    void testRandomUnvisitedNeighbor() {
        testRandomUnvisitedNeighbor1();
        testRandomUnvisitedNeighbor2();
        testRandomUnvisitedNeighbor3();
        testRandomUnvisitedNeighbor4();
        testRandomUnvisitedNeighbor5();
        testRandomUnvisitedNeighbor6();
        testRandomUnvisitedNeighbor7();
        testRandomUnvisitedNeighbor8();
        testRandomUnvisitedNeighbor9();
    }

    @Test
    void testRandomUnvisitedNeighbor1() {
        Cell testCell1 = labyrinth.randomUnvisitedNeighbor(labyrinth.getCell(0, 0));
        Assertions.assertTrue((labyrinth.getCell(1, 0) == testCell1)
            || (labyrinth.getCell(0, 1) == testCell1));
    }

    @Test
    void testRandomUnvisitedNeighbor2() {
        Cell testCell2 = labyrinth.randomUnvisitedNeighbor(labyrinth.getCell(5, 6));
        Assertions.assertTrue((labyrinth.getCell(4, 6) == testCell2)
            || (labyrinth.getCell(5, 5) == testCell2));
    }

    @Test
    void testRandomUnvisitedNeighbor3() {
        Cell testCell3 = labyrinth.randomUnvisitedNeighbor(labyrinth.getCell(0, 6));
        Assertions.assertTrue((labyrinth.getCell(0, 5) == testCell3)
            || (labyrinth.getCell(1, 6) == testCell3));
    }

    @Test
    void testRandomUnvisitedNeighbor4() {
        Cell testCell4 = labyrinth.randomUnvisitedNeighbor(labyrinth.getCell(5, 0));
        Assertions.assertTrue((labyrinth.getCell(5, 1) == testCell4)
            || (labyrinth.getCell(4, 0) == testCell4));
    }

    @Test
    void testRandomUnvisitedNeighbor5() {
        Cell testCell5 = labyrinth.randomUnvisitedNeighbor(labyrinth.getCell(5, 3));
        Assertions.assertTrue((labyrinth.getCell(5, 2) == testCell5)
            || (labyrinth.getCell(4, 3) == testCell5)
            || (labyrinth.getCell(5, 4) == testCell5));
    }

    @Test
    void testRandomUnvisitedNeighbor6() {
        Cell testCell6 = labyrinth.randomUnvisitedNeighbor(labyrinth.getCell(2, 6));
        Assertions.assertTrue((labyrinth.getCell(1, 6) == testCell6)
            || (labyrinth.getCell(3, 6) == testCell6)
            || (labyrinth.getCell(2, 5) == testCell6));
    }

    @Test
    void testRandomUnvisitedNeighbor7() {
        Cell testCell7 = labyrinth.randomUnvisitedNeighbor(labyrinth.getCell(2, 0));
        Assertions.assertTrue((labyrinth.getCell(2, 1) == testCell7)
            || (labyrinth.getCell(1, 0) == testCell7)
            || (labyrinth.getCell(3, 0) == testCell7));
    }

    @Test
    void testRandomUnvisitedNeighbor8() {
        Cell testCell8 = labyrinth.randomUnvisitedNeighbor(labyrinth.getCell(0, 3));
        Assertions.assertTrue((labyrinth.getCell(1, 3) == testCell8)
            || (labyrinth.getCell(0, 2) == testCell8)
            || (labyrinth.getCell(0, 4) == testCell8));
    }

    @Test
    void testRandomUnvisitedNeighbor9() {
        Cell testCell9 = labyrinth.randomUnvisitedNeighbor(labyrinth.getCell(3, 3));
        Assertions.assertTrue((labyrinth.getCell(2, 3) == testCell9)
            || (labyrinth.getCell(4, 3) == testCell9)
            || (labyrinth.getCell(3, 2) == testCell9)
            || (labyrinth.getCell(3, 4) == testCell9));
    }
}